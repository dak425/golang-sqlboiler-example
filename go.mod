module gitlab.com/dak425/golang-sqlboiler-example

go 1.15

require (
	github.com/friendsofgo/errors v0.9.2
	github.com/mattn/go-sqlite3 v1.14.1
	github.com/pkg/errors v0.9.1
	github.com/spf13/viper v1.7.1
	github.com/volatiletech/null/v8 v8.1.0
	github.com/volatiletech/randomize v0.0.1
	github.com/volatiletech/sqlboiler v3.7.1+incompatible
	github.com/volatiletech/sqlboiler/v4 v4.2.0
	github.com/volatiletech/strmangle v0.0.1
)

package main

import (
	"context"
	"database/sql"
	"fmt"

	_ "github.com/mattn/go-sqlite3"
	"github.com/volatiletech/sqlboiler/v4/queries/qm"
	"gitlab.com/dak425/golang-sqlboiler-example/models"
)

func main() {
	db, _ := sql.Open("sqlite3", "dev.db")
	ctx := context.Background()
	pilots, _ := models.Pilots().All(ctx, db)

	for _, p := range pilots {
		fmt.Printf("Pilot Info: %+v\n", p)
	}

	jets, _ := models.Jets().All(ctx, db)
	for _, j := range jets {
		fmt.Printf("Jet info: %+v\n", j)
	}
	jets, _ = models.Jets(qm.Where("color = ?", "white")).All(ctx, db)
	for _, j := range jets {
		fmt.Printf("Jet info: %+v\n", j)
	}
	pilots, _ = models.Pilots(qm.Limit(2), qm.Offset(0)).All(ctx, db)
	for _, p := range pilots {
		fmt.Printf("Pilot Info: %+v\n", p)
	}
	pilots, _ = models.Pilots(qm.Limit(2), qm.Offset(2)).All(ctx, db)
	for _, p := range pilots {
		fmt.Printf("Pilot Info: %+v\n", p)
	}
	//	var p models.Pilot
	//	p.Name = "Donny"
	//	p.Insert(ctx, db, boil.Infer())

}
